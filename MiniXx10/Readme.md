In collaboration with [Caroline Søe](https://gitlab.com/CarolineSA/aestetiskprogrammering), [Christina Krogsgaard](https://gitlab.com/Christinakrogsgaard/programmering), [Ditte Marie Damvad](https://gitlab.com/DitteMarieDamvad693372/aesthetic-programming) & [Josefine Pasma](https://gitlab.com/JosefinePasma/aestheticprogramming).

# Which sample code have you chosen and why?
We have chosen the code named “Handpose,” which is a program that detects the position of your hand. The program allows for palm detection and hand-skeleton finger tracking in the browser.  It can detect one hand at a time and provides 21 keypoints in 3D that describe important locations on the hand. We choose to examine this code because we think it is fascinating that the program is able to track the exact movement of the hand. We think the attention to detail is very cool and a bit scary, as it enlightens the endless possibilities for tracking every little thing shown on camera. 


# Have you changed anything in order to understand the program? If yes, what are they?
In order to understand what the syntax video.hide( ): means we deleted it, and discovered that it hides the original webcam video that is live recorded, and only projects recording webcam on top of the canvas, which is the place the hand is tracked. We also wanted to change the shape of the tracker. We started by changing the ellipse to a rect, after which we also changed the size. We also changed the colors to add a new meaning to “jazz hands”, as we made the dots blink in random colors. 

# Which line(s) of code is particularly interesting to your group, and why?

![](MiniX10.png)

We think that this line of code is interesting, partially because it is the one we can change most and because they use this “landmarks” thing which we at first did not understand what meant. Landmarks we now understand as the 3D coordinates of each hand landmark, meaning the different coordinates when moving your hand around. Hand is an object, and this landmarks is made in a separate file. 
 

# How would you explore/exploit the limitation of the sample code/machine learning algorithms?
One of the program's limitations is that it can only detect one hand at a time. 
We tried to move further from the camera to check how far it would capture all of the hand/finger movements and it proved to be quite good until we were around 4 meters from the camera. The right lighting is also at play for the program to work successfully - in a dark room it struggles when the hand is a bit far away. 
Obviously the hand needs to be in a fairly clear view as well, as it does not detect the hand when it is blocked by objects etc.  

# What are the syntaxes/functions that you don't know before? What are they and what have you learnt?
We aren’t really that familiar with the tracker function. We’ve only worked with it in class, where we didn’t write the code ourselves, but only copied an already-existing code. We’ve gotten a better understanding of this when analyzing the code - the ML5 library has a tracker in itself with some key points, and in the code, ellipses are drawn on these specific key points, visualizing the tracking-function.

# How do you see a border relation between the code that you are tinkering with and the machine learning applications in the world (e.g creative AI/ voice assistances/driving cars, bot assistants, facial/object recognition, etc)?
In Snapchat you can apply lots of filters on your face. Therefore there must be some sort of facial recognition that works similar to the “Handpose.” The Snapchat filters also have some of the same limitations and struggles for instance the filters works best if you look directly at the camera/phone and it also gets confused if you try to apply a filter (meant for one person), but you are two in the frame - Then it doesn’t know which face to chose and will jumps forth & back between the two persons. 

# What do you want to know more/ What questions can you ask further?
We would like to know more about how the tracking-function in the library is created, since this is somewhat out of our range of knowledge and abilities. In order for us to fully understand how the tracker works, there are endless questions we could ask. It would also be interesting to find a way to track both hands individually, without the points interfering with each other. 

# GIF OF THE PROGRAM

![](MiniX10.gif)

