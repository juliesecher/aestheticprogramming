let boyPosX;
let girlPosX;
let ballPosX;
let player1Score = 0
let player2Score = 0

function preload() {
  ball = loadImage("beachball.png");
  sound = loadSound("soundeffect.mov");
}

function setup() {
  createCanvas(800,800);
  background(142,218,255);
//direction of the ball at first (towards the girl).
  changeDirection = false;
//starting positions on the x-axis for the boy, girl and ball (this will be their x values.)
  boyPosX = 240
  girlPosX = 510
  ballPosX = 340

}

function draw() {
  background(142,218,255);
  noStroke();
//names of the upcoming functions.
  playBall();
  displayScore();
  checkResult();

//boat
  fill(250);
  triangle(120,575,240,575,120,460);
  rect(120,600,130,40);
  fill(255,0,0);
  rect(120,600,130,20);
  fill(0);
  rect(120,575,5,25);

//ball
  image(ball,ballPosX,465,60,60);

//ocean
  fill(25,180,255);
  rect(0,620,800,175);

//sand
  fill(255,225,125);
  rect(0,700,800,175);
//sun
  fill(255,255,0);
  circle(80,80,100);
  stroke(255,255,0);
  strokeWeight(5);
  line(140,110,200,150);
  line(115,140,180,220);
  line(80,150,80,210);
  line(15,220,45,140);
  line(140,80,220,80)
  noStroke();

//man w hat
  //player 1
  fill(0);
  textSize(12);
  textStyle(BOLD);
  text("player 1",boyPosX-20,400);
  //head
  fill(102,51,0);
  ellipse(boyPosX,465,55,70);
  //hat
  fill(0,204,102);
  ellipse(boyPosX,445,65,15);
  ellipse(boyPosX,430,45,30);
  //arm
  fill(102,51,0);
  ellipse(boyPosX+10,520,55,20);
  ellipse(boyPosX+33,490,12,50);
  //neck
  fill(102,51,0);
  rect(boyPosX-10,490,20,20);
  //t-shirt
  fill(253,107,84);
  rect(boyPosX-20,510,50,80);
  //arm
  fill(102,51,0);
  ellipse(boyPosX+20,520,55,15);
  ellipse(boyPosX+47,490,15,60);
  //shorts
  fill(87,186,0);
  rect(boyPosX-20,590,50,60);
  //legs and feet
  fill(102,51,0);
  rect(boyPosX-17,650,20,80);
  rect(boyPosX+8,650,20,80);
  rect(boyPosX+8,710,40,20);
  rect(boyPosX-15,710,40,20);
  //sunglasses
  fill(0);
  rect(boyPosX+10,460,20,10);
  rect(boyPosX-15,460,40,5);

//woman
  //player 2
  fill(0);
  textSize(12);
  textStyle(BOLD);
  text("player 2",girlPosX-30,400);
  //head and neck
  fill(255,209,138);
  ellipse(girlPosX,465,55,70);
  fill(255,209,138);
  rect(girlPosX-10,495,20,20);
  //arm and upper body
  ellipse(girlPosX-55,490,15,55);
  ellipse(girlPosX-30,515,55,15);
  rect(girlPosX-25,510,50,100);
  //bikini
  fill(204,0,204);
  rect(girlPosX-25,505,50,40);
  rect(girlPosX-25,590,50,30);
  //legs and feet
  fill(255,209,138);
  rect(girlPosX-23,620,20,105);
  rect(girlPosX+3,620,20,105);
  rect(girlPosX-40,705,20,20);
  rect(girlPosX-15,705,20,20);
  //arm
  ellipse(girlPosX-15,525,50,20);
  ellipse(girlPosX-35,500,15,50);
  //hair
  fill(255,232,13);
  ellipse(girlPosX-5,440,50,25);
  ellipse(girlPosX+20,480,25,75);
  //sunglasses
  fill(0);
  rect(girlPosX-30,460,20,10);
  rect(girlPosX-30,460,40,5);

//sandcastle
  fill(255,178,102);
  rect(630,680,70,40);
  rect(610,710,110,30);
  rect(595,740,140,30);
  triangle(640,680,665,620,690,680);
  rect(640, 685, 55, 55, 20);
  //flag
  fill(0);
  rect(635,650,5,30);
  fill(255,0,0);
  triangle(640,650,640,625,605,635);

//towel
  fill(0,204,102);
  rect(65,760,130,50);
  fill(255,51,153);
  rect(75,760,20,50);
  rect(105,760,20,50);
  rect(135,760,20,50);
  rect(165,760,20,50);

//rocks
  fill(120);
  ellipse(395,735,10,8);
  ellipse(400,760,10,8);
  ellipse(430,765,10,8);

//star fish
  fill(255,165,112);
  ellipse(429,722,10,22);
  rotate(2.0/2.0);
  ellipse(850,40,50,10);
  ellipse(850,40,10,50);

  }

function playBall() {
    //when the ball hits the x coordinate for the girl's arms:
    if(ballPosX>girlPosX-120) {
      //sound effect plays.
        sound.play(0.5);
      //girl/player 2 will get a point.
        player2Score++;
      //direction will change towards the boy.
        changeDirection=true

    //when the ball hits the x coordinate for the boy's arms:
    } else if (ballPosX<=boyPosX+50) {
      //sound effect plays.
        sound.play(0.5);
      //boy/player 1 will get a point.
        player1Score++;
      //direction will change towards the boy.
        changeDirection=false}

//makes the ball move.
//if the direction is false (towards girl), ball will move 1 up the x-axis every frame.
    if (ballPosX>=0 && changeDirection == false) {
        ballPosX=ballPosX+1
//if the direction is false (towards boy), ball will move 1 down the x-axis every frame.
    }	else if(changeDirection == true) {
      	ballPosX=ballPosX-1}

}

function keyPressed() {
  //when "A" is pressed, the boy's x-position will move 50 down the x-axis.
  if (keyCode == 65) {
    boyPosX-=50;
  //when "D" is pressed, the boy's x-position will move 50 up the x-axis.
} else if (keyCode == 68) {
    boyPosX+=50;
  //when left arrow is pressed, the girl's x-position will move 50 down the x-axis.
  } else if (keyCode == LEFT_ARROW) {
    girlPosX-=50;
  //when right arrow is pressed, the girl's x-position will move 50 up the x-axis.
  } else if (keyCode == RIGHT_ARROW) {
    girlPosX+=50;
  }
}

function displayScore() {
  fill(0);
  textSize(40);
  textStyle(BOLD);
  text("A DAY ON THE BEACH",230,100);
  textSize(15);
  text("control player 1 with the a + d key(s)",330,125);
  text("control player 2 with the left + right arrow key(s)",290,145);
  fill(255,255,0);
  textSize(20);
  //text will change everytime a player gets a point.
  text("player 1 has caught the ball " + player1Score + " time(s)",290,190);
  text("player 2 has caught the ball " + player2Score + " time(s)",290,220);
}

function checkResult() {
  //when player 1 has caught the ball more than 101 times, a winning screen will appear, stopping the game.
  if (player1Score > 101){
    fill(142,218,255);
    rect(200,30,500,200);
    fill(255,255,0);
    textSize(90);
    textStyle(BOLD);
    text("YAAAAY", 300, 240);
    textSize(20);
    text("you caught the ball more than 100 times :)",280,290);
    fill(0);
    text("player 1 is the winner!",380,265);
    noLoop();

    //when player 2 has caught the ball more than 101 times, a winning screen will appear, stopping the game.
  } else if (player2Score > 101){
    fill(142,218,255);
    rect(200,30,500,200);
    fill(255,255,0);
    textSize(90);
    textStyle(BOLD);
    text("YAAAAY", 300, 240);
    textSize(20);
    text("you caught the ball more than 100 times :)",280,290);
    fill(0);
    text("player 2 is the winner!",380,265);
    noLoop();

  }
}
