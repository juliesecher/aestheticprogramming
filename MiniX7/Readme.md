## **MINIX7 SUBMISSION - REVISIT THE PAST**

- [**Link for my MiniX7**](https://juliesecher.gitlab.io/aestheticprogramming/MiniX7/)
- [Link for my code for MiniX7](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX7/sketch.js)


**Which MiniX have I reworked and what have I changed?**

I have chosen to revisit my MiniX1 – “A Day on the Beach” - because I remember being very proud of it, and there were loads of elements in it, where I could change whatever, I wanted. In the original MiniX, I had made it possible to move around the ball between the two people on the beach, which was in some way a mini game, just without anything else really. This is what I wanted to build upon, using my new knowledge in coding. 

Instead of it being the ball you’re able to move around, it is now the two people playing the ball you’re controlling – it is a two-player game. You control the man with the a & d keys, and the woman with the left and right arrow keys. With the help of conditional statements, I have made the ball move on its own and change positions when hitting either the man or the woman. 

Even though it isn’t possible to drop the ball, I have still chosen to create a score-counter keeping track on how many times each player has caught the ball. Sometimes you’ll catch the ball more than once when playing, and the first one to catch it 100 times, will win the game. 

**What have I learnt when doing this MiniX?**

What I’ve realized when doing this MiniX, is how much I’ve actually learnt during this course. When doing my MiniX1, I didn’t _really_ understand what I was doing, and just tried my luck, but now I have a much bigger understanding for everything and was able to create a bit more of an advanced program. 

**How have I incorporated the concepts in my ReadMe/RunMe (the relation to the assigned readings)?**

I have considered the questions of politics in relation to things such as representation in my MiniXs, since more than one ethnicity is represented. This is basically the only thing, since my priority was to create something “light-hearted”, to stay in theme with the vibes of being on the beach for a day.  

**What is the relation between aesthetic programming and digital culture?**

Our everyday lives are highly influenced by digital culture, in the sense that software and/or hardware is something we carry around and use at all times (apps, websites etc.). For this reason, it is important and very relevant to be aware of what work has gone into the actual programming of these softwares. In that sense, we as users get a better understanding of the digital culture that affect our everyday lives, when we start to understand what actually goes into the programming.

**My new, revisited MiniX:**

![The new, revisited MiniX:](game.gif)

**My MiniX1:**

![My MiniX1:](MiniX1.gif)
