## **MINIX3 SUBMISSION** 

[Link to my MiniX3](https://juliesecher.gitlab.io/aestheticprogramming/MiniX3/)

[Link to my code for MiniX3](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX3/sketch.js)

**BASIC DESCRIPTION OF MY MINIX3**

I have created a flying pig as my throbber. I chose to make the background a light pink color, to go with the pig color scheme. Then, I wrote "when pigs fly..." in the center of my program, to refer to the saying _"When pigs fly"_ which is something you say to describe a situation that is impossible or never going to happen - but now that the pig is flying, anything is possible; and then comes the anticipation for what is gonna happen next! 😄 

**WHY DID I CHOOSE TO DO THIS SPECIFIC THROBBER?**

My initial thought was that it would be funny to do a random, flying animal as my throbber. I came up with the idea of the pig, because I created the light pink background, and thought that a pig would compliment it the best. As I was working on the pig, the above mentioned saying came to mind - and then I realised that it must've been fate! 

**WHAT DID I USE TO CREATE THIS THROBBER?**

I made the pig fly with the help of the infinite loop exercise we made in class - https://aesthetic-programming.net/pages/3-infinite-loops.html - but I chose to replace the yellow ellipses in the exercise with a pig with wings, created with the help of various geometric shapes. _(ellipse(), triangle() and circle() to be exact.)_ For the text, I used the basic _textSize(), textStyle(BOLD), and text() syntax'_. I chose to do them twice, to create a shadow-like effect. For the points/periods after the text, I chose to use a for-loop. I also used two of these, to match the shadow-effect from the text. To create the center the pig is flying around, I used _translate_ to move the origin. 

**WHAT DOES A THROBBER COMMUNICATE?**

A throbber usually communicates waiting. This is also what's happening in my case with this MiniX. You'll be occupied with watching the pig fly around and around, and know that at one point it will maybe stop, and you'll get to where you wish. It definitely creates some anticipation.

**REFLECTION** 

For my MiniX3, I've still primarily used the geometric shapes that I am starting to get very comfortable with, and in that sense I haven't really gone out of my comfort zone. However, it was challenging for me to make the pig fly around in a circle, and took me a while to figure that out. Initially, I could only make it fly backwards, and had to adjust the 360° degree to a -360° degree, to make it fly the right way. Besides that, I found the for-loop very challenging, and must admit that I am still not _quite_ sure how it actually works, or why you should use it, and not just do it in a more simple way - for example, I could have easily just done the points/periods with ellipses or circles. 

![](MiniX3.gif)

**REFERENCES**

- [ellipse()](https://p5js.org/reference/#/p5/ellipse)
- [triangle()](https://p5js.org/reference/#/p5/triangle)
- [circle()](https://p5js.org/reference/#/p5/circle)
- [fill()](https://p5js.org/reference/#/p5/fill)
- [text()](https://p5js.org/reference/#/p5/TEXT)
- [textSize()](https://p5js.org/reference/#/p5/textSize)
- [textStyle()](https://p5js.org/reference/#/p5/textStyle)
- [for-loop](https://p5js.org/reference/#/p5/for)
- [translate()](https://p5js.org/reference/#/p5/translate)
- [rotate(radians)()](https://p5js.org/reference/#/p5/rotate)

- [loop-exercise](https://aesthetic-programming.net/pages/3-infinite-loops.html)
