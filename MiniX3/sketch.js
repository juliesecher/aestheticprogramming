let o = 0;
let p = 0;

function setup() {
  createCanvas(800,800);
//number of frames to be displayed every second. Decides the speed of the pig.
  frameRate (11);
//removes the borders around the different objects.
  noStroke();
}

function draw() {
  background(255,213,228);

//name of upcoming function.
  drawElements();

//shadow for the text.
  fill(162,103,129);
  textSize(35);
  textStyle(BOLD);
  text("WHEN PIGS FLY",253,403);
//the text.
  fill(255);
  textSize(35);
  textStyle(BOLD);
  text("WHEN PIGS FLY", 250, 400);

//for-loop to create the shadow for the periods.
//p decides the distance of the periods.
//the 60 will decide the amount of periods.
  for(let p = 0; p < 60; p++){
    fill(163,103,129);
    ellipse(538+p,402,8);
    p += 20;
  }

//for-loop for the periods.
//o decides the distance of the periods.
//the 60 will decide the amount of periods.
for(let o = 0; o < 60; o++){
  fill(255);
  ellipse(538+o, 400, 8);
  o += 20;
}
}

function drawElements() {
//pigz is the amount of pigs (possible positions of the pigs), flying around on screen.
  let pigz = 30;
  push();
  //moves the origin to the center of the canvas.
  translate(400,400);

// -360/pigz is the degree of the movement of the pig (360 degrees makes it fly the wrong way round).
// frameCount is the amount of frames that have been displayed since the program started.
// frameCount%pigz decides the next, possible position for the pig, according to the remainder.
  let pig = -360/pigz*(frameCount%pigz);
  rotate(radians(pig));
  noStroke();

//hind-wing.
  fill(225);
  ellipse(162,300,20,30);
//hind-legs and ear.
  fill(162,103,129);
  ellipse(180,345,10,20);
  ellipse(157,345,10,20);
  triangle(185,298,190,288,195,298);
//body, legs, head, snout, and tail.
  fill(255,154,188);
  ellipse(165,325,55,40);
  ellipse(150,345,10,20);
  ellipse(175,345,10,20);
  circle(190,310,35);
  ellipse(207,312,15,10);
  ellipse(210,312,8,9);
  ellipse(135,320,20,5);
  ellipse(127,326,5,15);
  ellipse(130,332,10,5);
  ellipse(134,322,5,25);
//nostrils.
  fill(0);
  ellipse(209.5,312,0.5,3);
  ellipse(211.7,312,0.5,3);
//eye + mouth.
  fill(255);
  circle(195,305,5,5);
  fill(0);
  circle (195,305,1);
  ellipse(200,320,2,4);
//ear.
  fill(255,154,188);
  triangle(182,298,185,288,190,298);
//wing.
  fill(255);
  ellipse(158,300,20,30);

pop();

}
