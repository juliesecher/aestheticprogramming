## (AT LEAST) YOU ARE NOT IN WAR

**[Link for our MiniX8](https://juliesecher.gitlab.io/aestheticprogramming/MiniX8/)**

[Link for the code](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX8/sketch.js)

**ReadMe/Vocable Code**

We have created a work that illustrates the big contrasts between our daily life and the life during war, that now is the reality for the Ukrainian people, who used to live a life similar to ours. We wanted to highlight how we everyday take things & the privileged life we have for granted. We complain about stuff that can’t even be categorized as a real problem, while there are many people who have had their life flipped upside down, and would wish to only have as small problems as us. We do not know how lucky we truly are or anyhow sometimes we forget how lucky we are. 

### **What is the problem you want to pose/understand (not solve) via this minix?**

Through this miniX we want to bring attention to the situation in Ukraine. We have used words to provoke thoughts and illustrate a vast contrast between different lives in similar countries. Coming from a very privileged country with minimal to no problems, it is difficult to see and understand their situation and how they must feel. Due to this, many people are just living their normal life without putting much thought into what is happening in Ukraine or other places in the world for that matter. We want to confront how, “It doesn't affect us, so it doesn’t concern us” sometimes is a natural reaction to incidents like this. It is so easy to turn a blind eye to all the horrible things going on in other places of the world. Of course we shall & can’t not pause our lives entirely. We will never be able to fully understand their situation, but nonetheless we still think it’s important to stop for a second and break down the surreal feeling we walk around with, to realize that this is actually happening and consider that it could have been happening for us, even though it seems unlikely. No one had foreseen that this would happen to Ukraine either. Send some love & help their way!


### **Describe how your program works, and what syntax you have used, and learnt?**

We have learned to create and implement a JSON file in our code. Within our JSON file we have 22 sentences describing “first world problems” and 17 sentences describing the reality the Ukrainian people are facing right now. The sentences are set up in an array, because we want the program to show one sentence at a time and shift between them with the frameRate(2). We have created two arrays; one for the “first world problems” and one for the current problems for the Ukrainian people. While the program is running there is at all time one first-world-problem & one war-problem displayed on the screen. In the background we have used a picture of a destroyed/bombed building from Ukraine. We have uploaded this photo with the function preload(). We also preloaded the JSON file in order to use it in our sketch.

### **How would you reflect on and analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language)?**

We think our work mostly speaks into Vocable Code in terms of the message in our work, that is most impressionable in our running program and not in the code itself. This message is related to the quote by Winnie Soon & Geoff Cox 
>>>
“Vocable Code has a direct relation to bodily practices, the act of voicing something, and how the voice resonates with political practices.” (Soon & Cox, 2020, p.182)
>>>
The sketch code itself is a regular code and is built after the conventional way of doing it. In the JSON file, which holds all of our statements, the message lies more hidden and as an observer you will have to interpret it yourself. We believe that the message gets more clear, when two statements are put opposed to each other in the program. In the code the sentences are separated into arrays, so in that way you will still understand the division and maybe spot the contrast between the statements. 
Our JSON code acts as a script for the ‘performance’ when the code is executed in the program.Just by looking at the JSOn code, you can say it is ready to do something and that it says what it will do and does it at the same time.  We think this qoute from the book describes that:
>>>

“Code is both script and performance, and in this sense is always ready to do something: it says what it will do, and does it at the same time.” (Soon & Cox, 2020, p.169)

>>>

However, we think the whole performance and the essence of it is best acknowledged when the program is run. The JSON code is seen in a new light when the two sections are put against eachother. 

![](MiniX8.gif)
