let data;
let world;
let u;
let blink = 0

function preload() {
  //loads the data from the JSON-file.
  data = loadJSON("problems.json");
  //image for the background.
  b = loadImage("u.png");
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  //speed of the program.
  frameRate(2);
}

function draw() {
  background(b);
  //"world" is the first world problems in the JSON-file.
  world = data.WorldP;
  //"u" is the Ukraine-problems in the JSON-file.
  u = data.Ukraine;

  //the i variable chooses a random number from 0-21.
  let i = floor(random(0,21))
  //the j variable chooses a random number from 0-16.
  let j = floor(random(0,16))

  //with the help of the variables above, statement will be a random statement from the first world problems.
  let statement = world[i]
  //with the help of the variables above, statement will be a random statement from the first world problems.
  let statement2 = u[j]

  //the boxes.
  noStroke();
  fill(255,180);
  rect(80,35,1800,800);
  fill(0,180);
  rect(110,133,500,30);
  rect(110,433,500,30);

  //the text.
  push();
  fill(255,0,0);
  textFont("Courier New");
  textSize(40);
  textStyle(BOLD);
  text("It's horrible when...", 110,150);
  //random first world problem (line 32).
  text(statement, 390, 230)
  text("But at least you...", 110,450);
  //random Ukraine problem (line 34).
  text(statement2, 390, 530);
  pop();

  push();
  //blink will go up with 1 every frame.
  blink += 1;
  //if the remainder, after blink has been divided by 6, is less or equal to 4, the text will be red.
  if(blink % 6 <= 4){
  fill(255,0,0);
  textFont("Courier New");
  textSize(50);
  textStyle(BOLD);
  text("... YOU ARE NOT IN WAR.", 150,750);
  pop();
  //if the remainder, after blink has been divided by 6, is more than 4, the text will be "invisible". 
  } else {
  fill(255,0);
  textFont("Courier New");
  textSize(50);
  textStyle(BOLD);
  text("... YOU ARE NOT IN WAR.", 150,750);
  pop();
  }
}
