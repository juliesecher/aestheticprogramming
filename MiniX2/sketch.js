function setup() {
  createCanvas(800,800);
//removes the borders around everything.
  noStroke()
}

function draw() {
//ocean-color.
  background(137,200,255);

//sand
  fill(235,209,150);
  ellipse(400,795,1000,300);
  fill(186,162,106);
  ellipse(335,675,350,30);

//turtle
  //hind legs
  fill(0,204,102);
  ellipse(240,650,45,60);
  ellipse(255,670,50,30);
  ellipse(425,650,45,60);
  ellipse(440,670,50,30);
  rect(400,570,40,60);
  //front legs
  fill(51,255,153);
  ellipse(200,650,45,60);
  ellipse(215,670,50,30);
  ellipse(392,650,45,60);
  ellipse(410,670,50,30);
  ellipse(175,570,30,10);
  rect(180,570,230,60);
  //neck + head
  quad(410,601,486,520,440,480,400,576);
  ellipse(470,495,100,80);
  //turtle shield????
  fill(0,102,51);
  ellipse(290,555,235,100);
  //eye + mouth
  fill(0);
  circle(460,480,15);
  fill(153,51,51);
  ellipse(500,505,40,5);
//straw
  fill(255,255,0);
  rect(540,400,130,10);
  rect(660,400,10,50);
  fill(216,195,0);
  ellipse(538,405,5,10);
  ellipse(665,452,10,5);

//when mouse is pressed down, the following things will pop up:
if (mouseIsPressed === true) {
//cover-up for previous straw + eye
  fill(137,200,255);
  ellipse(530,400,280,60);
  ellipse(670,435,40,70);
  fill(51,255,153);
  circle(460,480,15);
//straw in mouth
  fill(255,255,0);
  rect(502,508,10,50);
  fill(216,195,0);
  rect(502,555,10,5);
//straw in throat
  fill(51,255,153);
  rect(400,500,30,10);
//cross over the eye.
  fill(0);
  rect(445,475,30,7);
  rect(455,463,7,30);
//cover-up for previous straw
  fill(137,200,255);
  ellipse(530,400,280,60);
  ellipse(670,435,40,70);
//tongue
  fill(153,51,51);
  ellipse(485,512,10,15);
}
}
