## **MINIX2 SUBMISSION** 

[Link to my MiniX2](https://juliesecher.gitlab.io/aestheticprogramming/MiniX2/)

[Link to my code for MiniX2](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX2/sketch.js)

**BASIC DESCRIPTION OF MY MINIX2**

For my second MiniX, I chose to stay in theme with my MiniX last week - the sea. 
Although, for this week, I chose to make the theme a bit more "dark". I have created two turtle-emojies, who live under the sea. 
The first turtle is standing on the bottom of the ocean, minding its own business, while a yellow straw - made of plastic, used by an evil human being - is floating in front of the turtle, threatening its peace. With the press of a mouse, the future of the precious, innocent turtle is portrayed _(this is where the second turtle comes into play)_ - the vicious straw has made it into its throat, strangling and killing it. 

**WHY DID I CHOOSE TO DO THESE SPECIFIC EMOJIES?**

I chose to focus on the reality of plastic disposal in the ocean. I thought it was important to shed light onto this issue as well as other social and cultural issues, since it is something happening everyday -- and is a _very_ relevant, political issue/discussion. It is also societally relevant, since different companies are starting to use and/or sell paper straws instead of plastic to try and prevent this issue. 

**WHAT DID I USE TO CREATE MY EMOJIES?**

For the turtles I used _ellipse()_ and _rect()_ to create the shields and the feet/legs. 
These are also what I used to create the straw. 
For the neck I used a quad, to really get the feel of a real, long turtle's neck.
I made used the _fill()_ syntax, to color it all in. 
To get the turtle to switch from one to the other, I used _(mouseIsPressed === **true**)_. 
I wrote in the items/things I wanted to appear, under the mouseIsPressed syntax; in that way, when the mouse is pressed, these objects will appear - and when you let go, they will dissapear, making the turtle go back to the default. 

**REFLECTION OF MY PROGRAMMING EXPERIENCE SINCE LAST MINIX**

Since my MiniX1 I've definitely gotten a bigger understanding for what I am actually doing. Also, I'm getting into a sort of "routine" with the various shapes and coordinates, making my experience with coding a lot more entertaining. I am excited for what is in store for the future MiniX'. 

### **GIF OF MY MINIX2**

![](MiniX2.gif)

**REFERENCES**
- [ellipse()](https://p5js.org/reference/#/p5/ellipse)
- [rect()](https://p5js.org/reference/#/p5/rect)
- [fill()](https://p5js.org/reference/#/p5/fill)
- [circle()](https://p5js.org/reference/#/p5/circle)
- [mouseIsPressed()](https://p5js.org/reference/#/p5/mouseIsPressed)



