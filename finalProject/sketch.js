let reasons;
let adSound;
let advert;
let googleI;
let clo;
let scroll;
let nameInput;
let quota;

//Pictures, sound effect and the JSON File.
function preload(){
  data = loadJSON("reasons.json")
  google = loadImage("googleHomepage.png");
  ad = loadImage("satanisk.png");
  back = loadImage("back.png")
  googleI = loadImage("GoogleI.png")
  adSound = loadSound("adsound.mp3")
  blurBack = loadImage("googleHomepage1.png");
  avatar = loadImage("avatar.png");
  quota = loadFont("Quota-Regular.otf");

}

function setup(){
  createCanvas(windowWidth, windowHeight);
  background(back);

//The Google icon, when clicked you get into our Google web-browser.
  googleI = createImg("GoogleI.png")
  googleI.size(width/19.5,height/14.6)
  googleI.position(width/2.17, height/1.105)
//When googleI is pressed the function chromeLogIn will start.
  googleI.mousePressed(chromeLogIn);

//text-box for log in.
  nameInput = createInput();
  nameInput.position(width/2.45, height/1.8);
  nameInput.size(250,50);
//is not shown at first.
  nameInput.style('display', 'none');
  nameInput.style('font-size', '25px');

//button for logging in
  logInButton = createButton('SIGN IN');
  logInButton.position(width/2.22,height/1.6);
  logInButton.style("background","#72009B")
  logInButton.style("color", "#ffffff");
  logInButton.size(150, 40);
  logInButton.style('font-size', '20px');
//is not shown at first.
  logInButton.style('display', 'none');
//When LogInButton is pressed, the function googleScreen will start.
  logInButton.mousePressed(googleScreen);

// The 'why am i seeing this' button.
  advert = createButton("Why am I seeing this?");
  advert.style("background", "#c8c8c8");
  advert.style("color", "#ffffff");
  advert.size(width/8, height/32);
  advert.position(width/1.62, height/2.35);
// When advert is pressed, the function theAlgorithm will start.
  advert.mousePressed(theAlgorithm);
//advert is not shown at first.
  advert.style('display', 'none');

// The red "X" button on the window
  clo = createButton("X")
  clo.style("background", "#ed1118");
  clo.style("color", "#ffffff");
  clo.size(width/45, height/35);
  clo.position(width/1.28, height/8);
// is not displayed at first.
  clo.style('display', 'none');

// The scroll bar
  //(minimum, maximum, and start value of scrollbar).
  scroll = createSlider(1, 45, 1);
  scroll.position(width/1.67, height/2);
  //rotated 90 degrees to make is vertical instead of horizontal.
  scroll.style("transform","rotate(90deg)");
  scroll.style("width","550px");
  //when the mouse is released on the scrollbar, the function scrollBar starts.
  scroll.mouseReleased(scrollBar);
  //is not shown at first.
  scroll.style('display', 'none');

}
//makes it possible to use enter to log in.
function keyPressed() {
  if (keyCode === 13) {
    googleScreen();
  }
}

function chromeLogIn() {
  //button is hidden when this function is started.
  googleI.hide();

//blurred background to create the effect of a log-in screen.
  push();
  imageMode(CENTER);
  image(blurBack, width/2, height/2, width/1.18, height/1.1);
  pop();

//avatar for the log-in.
  push();
  imageMode(CENTER);
  image(avatar,width/2, height/2.7, width/8.5, height/4.7);
  pop();

  push();
  textAlign(CENTER)
  fill(10);
  textSize(30);
  text("TYPE IN YOUR NAME" ,width/2, height/1.95);
  textSize(20);
  text("TO LOG IN TO YOUR GOOGLE ACCOUNT",width/2, height/1.85);
  pop();

//makes the log-in button and text-box appear.
  logInButton.show();
  nameInput.show();

}

function googleScreen() {

//hides the text-box and log-in button.
  nameInput.hide();
  logInButton.hide();

//the Google screen is displayed.
  push();
  imageMode(CENTER);
  image(google, width/2, height/2, width/1.18, height/1.1);
  pop();

//sound effect for the ad
  adSound.play();

//interval makes the ad appear with a 1.5 second delay.
  adShower = setInterval(adShower, 1500);
}

function adShower(){
//the advertisement itself.
  push();
  rectMode(CENTER);
  fill(200);
  stroke(140);
  rect(width/2, height/1.46, width/2, height/1.92,10);

  imageMode(CENTER);
  image(ad, width/2, height/1.44, width/2-20, height/2-20);
  pop();

  noStroke();
  fill(255);
  textSize(15);
  text("Advertisement", width/2.15, height/2.24)

//"Why am I seeing this" button is displayed.
  advert.show();
}

function theAlgorithm(){

//clearInterval stops the ad from printing every 1.5 seconds :-)
  clearInterval(adShower);
//the "why am I seeing this button" is hidden.
  advert.hide();

//this is the input you get at first when logging in with the textbox (the name you type).
  name = nameInput.value();

//window for the data log
  push();
  rectMode(CENTER);
  fill(10);
  stroke(140);
  rect(width/2, height/2, width/1.6, height/1.3,10);
  pop();

  fill(0,255,0);

//giving a name to the arrays from the JSON-file.
  reasons = data.reasonsWhen;
  reasons1 = data.reasonsWhat;

    push();
    let i = 0;
  //if t is smaller or equal to the height of the data log window, t will plus with 1.
  //i is the specific reason in the array that is displayed.
    for (let t = 0; t <= height/1.48; t++) {
      textFont(quota);
      text(reasons[i], width/4.6, height/5+t);
      textFont("Courier New");
      text("You" + reasons1[i], width/2.5, height/5+t);
//t is the distance of the reasons.
      t += 25
//i increases with 1 every frame, making it display different reasons in the array.
      i += 1
   }

//box for the DATA LOG text.
  pop();
  push();
  fill(50);
  rectMode(CENTER);
  rect(width/2.1, height/6.6, width/1.8, height/20,10)
  fill(225);
  noStroke();
  textSize(40);
//name is the value of the text box in the beginning (line 175).
  text("Data log for " + name + ":", width/4.6,height/6);
  pop();

//scrollbar and the red x is displayed now.
  scroll.show();
  clo.show()

}

function scrollBar() {
//new data-log window.
  push();
  fill(10);
  rectMode(CENTER);
  rect(width/2, height/1.85, width/1.6, height/1.4,10);
  pop();

  textFont("Courier New");
  fill(0,255,0);

//if u is smaller or equal to the height of the data log window, u will plus with 1.
  for (let u = 0; u <= height/1.48; u++){
      textFont(quota);
//when the scrollBar is released with the mouse, a random list of reasons will be generated each time.
      text(random(reasons),width/4.6, height/5+u);
      textFont("Courier New");
      text("You" + random(reasons1), width/2.5, height/5+u);
//u is the distance between the reasons.
      u += 25
  }
}
