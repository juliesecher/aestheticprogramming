
function setup() {
  createCanvas(800,800);

}

function draw() {
//color of the sky.
  background(142,218,255);
//removes black borders around everything.
  noStroke();

//sand
  fill(255,255,155);
  rect(0,700,800,175);
//ocean
  fill(0,0,255);
  rect(0,600,800,100);
//sun
  fill(255,230,0);
  circle(65,80,90);

//sharks in the water.
  fill(100);
  triangle(640,650,675,650,670,610);
  triangle(710,625,765,675,710,675);

//boat
  fill(250);
  triangle(400,555,520,555,400,440);
  rect(400,580,130,20);
  fill(255,0,0);
  rect(400,580,130,20);
  fill(0);
  rect(400,555,5,25);

//sandcastle
  fill(255,178,102);
  rect(580,680,70,40);
  rect(560,710,110,30);
  rect(545,740,140,30);
  triangle(590,680,615,620,640,680);
  rect(590, 685, 55, 55, 20);

  //windows for the sandcastle.
  fill(201,120,0);
  rect(607,680,15,30,20);
  rect(595,682,10,20,20);
  rect(624,682,10,20,20);
  rect(624,715,10,20,20);
  rect(595,715,10,20,20);
  rect(610,715,10,20,20);
  rect(570,745,10,20,20);
  rect(585,745,10,20,20);
  rect(600,745,10,20,20);
  rect(615,745,10,20,20);
  rect(630,745,10,20,20);
  rect(645,745,10,20,20);

  //flag for sandcastle
  fill(0);
  rect(585,650,5,30);
  fill(255,0,0);
  triangle(590,650,590,625,555,635);

//clouds
  fill(250);
  circle(200,220,70);
  circle(220,250,70);
  circle(260,250,70);
  circle(260,220,70);
  circle(300,230,70);
  circle(680,120,70);
  circle(590,120,70);
  circle(640,100,70);
  circle(630,130,70);
  circle(680,400,70);
  circle(590,420,70);
  circle(640,400,70);
  circle(630,430,70);

//surf board
  fill(255,137,0);
  ellipse(730,600,80,20);
  fill(255,216,193);
  rect(700,540,65,8);
  rect(720,560,8,40);
  rect(735,560,8,40);
  ellipse(730,518,20,40);
  fill(0,102,51);
  rect(718,538,25,35);

//making the beach ball move.
//x and y are the starting points for the ball on the x and y axis.
let x = 185;
let y = 470;
//when left arrow is pushed, x will move 50 down the x-axis.
  if (keyIsDown(LEFT_ARROW)) {
    x -= 50;
  }
//when right arrow is pushed, x will move 50 up the x-axis.
  if (keyIsDown(RIGHT_ARROW)) {
    x += 48;
  }
//when down arrow is pushed, y will move 200 up the y-axis.
  if (keyIsDown(DOWN_ARROW)) {
    y += 200;
  }
//when up arrow is pushed, y will move 150 down the y-axis.
  if (keyIsDown(UP_ARROW)) {
    y -= 150;
  }
//beach ball.
// x and y values makes it possible for the ball to move because of the conditional statements above.
  fill(0,255,0);
  circle(x,y,65);
  fill(255,154,0);
  ellipse(x,y,65,35);
  ellipse(x,y,65,45);
  fill(255,0,1);
  ellipse(x,y,65,15);

//man w hat
  //head
  fill(102,51,0);
  ellipse(65,465,55,70);
  //hat
  fill(0,204,102);
  ellipse(65,445,65,15);
  ellipse(65,430,45,30);
  //arm
  fill(102,51,0);
  ellipse(75,520,55,20);
  ellipse(98,490,12,50);
  //neck
  fill(102,51,0);
  rect(55,490,20,20);
  //t-shirt
  fill(253,107,84);
  rect(45,510,50,80);
  //arm
  fill(102,51,0);
  ellipse(85,520,55,15);
  ellipse(112,490,15,60);
  //shorts
  fill(87,186,0);
  rect(45,590,50,60);
  //legs and feet
  fill(102,51,0);
  rect(48,650,20,80);
  rect(73,650,20,80);
  rect(73,710,40,20);
  rect(50,710,40,20);
  //sunglasses
  fill(0);
  rect(75,460,20,10);
  rect(50,460,40,5);

//woman
  //head and neck
  fill(255,209,138);
  ellipse(325,465,55,70);
  fill(255,209,138);
  rect(315,495,20,20);
  //arm and upper body
  ellipse(270,490,15,55);
  ellipse(295,515,55,15);
  rect(300,510,50,100);
  //bikini
  fill(204,0,204);
  rect(300,505,50,40);
  rect(300,590,50,30);
  //legs and feet
  fill(255,209,138);
  rect(302,620,20,105);
  rect(328,620,20,105);
  rect(285,705,20,20);
  rect(310,705,20,20);
  //arm
  ellipse(310,525,50,20);
  ellipse(290,500,15,50);
  //hair
  fill(255,232,13);
  ellipse(320,440,50,25);
  ellipse(345,480,25,75);
  //sunglasses
  fill(0);
  rect(295,460,20,10);
  rect(295,460,40,5);

//ice cream
  fill(90,80,0);
  rect(250,750,20,5);
  fill(153,153,255);
  rect(265,745,25,15);
  rect(275,745,25,10);

//towel
  fill(0,204,102);
  rect(65,760,130,50);
  fill(255,51,153);
  rect(75,760,20,50);
  rect(105,760,20,50);
  rect(135,760,20,50);
  rect(165,760,20,50);

//rocks
  fill(120);
  ellipse(395,735,10,8);
  ellipse(400,760,10,8);
  ellipse(430,765,10,8);

//star fish
  fill(255,165,112);
  ellipse(429,722,10,22);
  rotate(2.0/2.0);
  ellipse(850,40,50,10);
  ellipse(850,40,10,50);

}
