### **Julie MiniX1 submission**

[Link to MiniX1](https://juliesecher.gitlab.io/aestheticprogramming/MiniX1/)

[Link to code](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX1/sketch.js)

For my first MiniX I decided to portray a beach on a summer day. On the beach I have - with the help of various geometrical shapes and loads of different colors - created a lot of beach-related objects to really set the "beach mood". These objects consist of a half-eaten ice cream, intimidating sharks, a surfer and boat in the water - and of course, an exquisite sandcastle.

However, the main focus point in my MiniX is the two people with the colorful beach ball, standing in the foreground. I decided very early on in my programming process that I wanted something to move - and quickly decided that the two people should be able to play with the beach ball. I played around with different Syntax' to make this possible, and ended up with the "let", If" and "keyIsDown syntax'.

I decided on some coordinates for the ball to have its starting point:
 > "let x = 185"
 > "let y = 470"

 Afterwards, I chose how much the ball should be able to move on the x and y axis' when specific keys are pressed.
> "if (keyIsDown(LEFT_ARROW))
  x -= 50;

> if (keyIsDown(RIGHT_ARROW))
  x += 48;

> if (keyIsDown(DOWN_ARROW))
  y += 200
  
> if (keyIsDown(UP_ARROW))
  y -= 150;)"

This basically means that when the left arrow key is pressed, the ball will move negative 50 down the x axis, when the right is pressed it will move 48 up the x axis - and so on.
Therefore, you are able to press all four Arrow-Keys to move the ball to the left, right, up and down. When creating the ball, instead of putting in the x & y coordinates, I just put "x & y" to refer to the keyIsDown Syntax.

My first programming experience was very interesting and I had a lot of fun with playing around with the different shapes and colors. I did it with the help of the following Syntax':
- function setup ()
- function draw ()
- fill()
- rotate
- noStroke
- keyIsDown

For the various shapes I've used these:
- rect()
- ellipse()
- circle()
- triangle()

![](MiniX1.gif)
