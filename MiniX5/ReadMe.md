## **MINIX5 - A GENERATIVE PROGRAM - SUBMISSION** 

[Link to my MiniX5](https://juliesecher.gitlab.io/aestheticprogramming/MiniX5/)

[Link to my code for MiniX5](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX5/sketch.js)


**DESCRIPTION OF MY MINIX5**

For this weeks' MiniX, we had to create a generative program, where we set a couple of rules that we had to implement into the program itself. At first, I found it very difficult to actually decide on the rules, but knew that I wanted to do something 'chaotic' and colorful. Therefore, I decided on the following rules; 

1 - When the random-syntax chooses a number higher than 0.5, it will draw a circle in a random color, location, and size.

2 - When it chooses a number lower than 0.5 instead, it will draw a random letter from the alphabet - also in a random color, location, and size.

So basically, my MiniX5 is a generative program, where it starts out with a black canvas with a _while-loop_, creating a starting circle in a random color. As time passes, circles and letters in random colors and letters appear, covering the black canvas in loads of different colors.

**WHAT DID I USE TO CREATE MY MINIX5?**

For this MiniX, I used the _random_-syntax a lot. I used it to decide whether the program should draw a circle or a letter. Furthermore, it was the _random_-syntax that made the decisions on what the colors and the location of the various objects should be. For my conditional statement, I used an _if-else_-syntax, also deciding whether or not the program should draw a circle or a letter. For my loop, I chose to do a _while_-loop, which created my starting circle. I also used an array for all the letters in the alphabet, so I could make the program draw them in a random order. Additionally, I've used the frameRate, to decide how fast I wanted my program to run.

**HOW HAS THIS MINIX HELPED ME, IN TERMS OF UNDERSTANDING GENERATIVE ART?**

I have definitely realised how "little" control you have as an artist in generative art. It is the randomness of it all that actually creates the art - which I find to be very interesting. My program was very simple, and therefore I think that it was more predictable than it could've been, had I created some more advanced rules. 

**REFLECTIONS**

The biggest issue I ran into with the MiniX5, was actually coming up with an idea for the program itself (especially coming up with the rules). Additionally, I still don't understand why it is necessary to keep the _"bip = bip -20"_ line for my program to work, since it shouldn't really have that big of an impact. All I know is that my program wouldn't run without it, so I just decided to keep it in (I also tried asking for help in the instructor-classes, where we tried to use the _console.log_, but we couldn't seem to figure it out).

![](MiniX5.gif)

**REFERENCES**

- [circle()](https://p5js.org/reference/#/p5/circle)
- [fill()](https://p5js.org/reference/#/p5/fill)
- [text()](https://p5js.org/reference/#/p5/TEXT)
- [textSize()](https://p5js.org/reference/#/p5/textSize)
- [if-else](https://p5js.org/reference/#/p5/if-else)
- [frameRate](https://p5js.org/reference/#/p5/frameRate)
- [while-loop](https://p5js.org/reference/#/p5/while)
- [console.log](https://p5js.org/reference/#/console)
- [arrays](https://p5js.org/reference/#/p5.Vector/array)
- [random](https://p5js.org/reference/#/p5/random)
