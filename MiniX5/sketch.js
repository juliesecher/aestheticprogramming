let bip = 40;
//array with letters from the alphabet.
let alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","Y","Z","Æ","Ø","Å"]

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(80);
}

function draw() {
  noStroke();
//chooses a random number between 0 and 1.
//if this number is smaller than 0.5, the program will print a circle in a random color.
//if this number is bigger than 0.5, the program will print a letter from the alphabet in a random color.
if (random(1) < 0.5) {
  fill(random(255),random(255),random(255));
  circle(random(width),(random(height)), random(20,70));
} else {
  textSize(random(10,50));
  text(alphabet[floor(random(28))],random(width),random(height));
  }

//creates the starting circle in a random color.
while (bip >= 20) {
// subtracts 20 from bip, making the bip value equal 20.
// shouldn't have any meaning in relation to the program itself, but it doesn't work without this line.
  bip = bip - 20;
  fill(random(255),random(255),random(255));
  circle(200,200,80);


}
//used this to try and figure out why the 'while' looped did what it did.
console.log(bip);
}
