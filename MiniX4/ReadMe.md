## **MINIX4 SUBMISSION** 

[Link to my MiniX4](https://juliesecher.gitlab.io/aestheticprogramming/MiniX4/)
(maybe you need to reload the page after having given access to webcam, for it to work properly).

[Link to my code for MiniX4](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX4/sketch.js)

**BASIC DESCRIPTION OF MY MINIX4**

When opening my MiniX, you're met with nothing but a big, red button. On the button, it says that you should push it, and then _all_ data the government has of you, will be deleted. However, the second you push the red button, your webcam will take a photo of you, and you will be accused of trying to delete government property - and will be told that the photo will be sent directly to the head of authority. 

**WHY DID I CHOOSE TO DO THIS FOR MY MINIX4?**

The theme for this week's MiniX was Data Capture. The assignment was to play around with different kinds of data gathering, especially in relation to the "Capture All" transmediale open call. My assumption for this week was that the MiniX had to be a sort of _'critical'_ design, and therefore I chose to do something related to online surveillance. What I find kind of ironic regarding my MiniX is that you're actually trying to delete all the data the goverment has of you, but doing so, you will actually be gifting them even more data - that is the photo in question. I chose to make the button red to make it seem kind of "dangerous" in a way, but still with the intention of making people want to push it.

**WHAT DID I USE TO CREATE MY MINIX4?**

Creating my MiniX4, I got a lot of inspiration from the exercise we did in class; https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class05, where we played around with the different ways of data-capturing _(audio, webcam, mouse, keyboard etc.)_ and things such as basic buttons. I created the button using _createButton()_, and made it red with the help of the _style()_ syntax, where I used the HEX-color codes to get the red color. For the shape of the button I used _button.style("border-radius","50%")_, which makes it round. Afterwards, I used the _button.mousePressed(change)_ syntax, to create a _change function()_, so when the button is clicked, something will actually happen. Within the _function change()_. And then I had the _image_ syntax, making the webcam take an image when the button is pushed as well. Lastly, I used the _text_ syntax to write the threatening message.

**REFLECTIONS** 

For this MiniX I stepped out of my comfort zone, since I didn't really use any of the simple, geometrical shapes I've gotten comfortable with in the previous MiniX's. I found it quite difficult at first to make all the different data-gathering-functions work, but after changing some things in my HTML-file, I finally made it work. The biggest obstacle I ran into was when I wanted to turn on the webcam after having pushed the button. I tried loads of different things; creating a rectangle in front of the whole screen, trying to make it dissapear when the button was pushed and other, random, ridiculous things, but no matter what I tried, I couldn't get it to actually be a "live-feed" of the webcam, and could only make it take a photo. I tried getting help in the instructor-classes, but we couldn't make it work, no matter what we did. I then chose to just run with the photo, and I actually ended up liking it better, than my original idea.


![](MiniX4_1_.png)
![](MiniX4_2_.png)

**REFERENCES**

- [fill()](https://p5js.org/reference/#/p5/fill)
- [text()](https://p5js.org/reference/#/p5/TEXT)
- [textSize()](https://p5js.org/reference/#/p5/textSize)
- [textStyle()](https://p5js.org/reference/#/p5/textStyle)
- [createButton()](https://p5js.org/reference/#/p5/createButton)
- [style()](https://p5js.org/reference/#/p5.Element/style)
- [mousePressed()](https://p5js.org/reference/#/p5.Element/mousePressed)
- [createCapture()](https://p5js.org/reference/#/p5/createCapture)
- [image()](https://p5js.org/reference/#/p5/image)

- [exercise from class where I got inspiration](https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class05)
