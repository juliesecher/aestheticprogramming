let camcorder;
let button;

function setup() {
  createCanvas(640,480);
  background(255);
  noStroke();

//button
  //creates a button with the specific text.
  button = createButton("CLICK HERE TO ERASE ALL DATA THE GOVERNMENT HAS OF YOU!");
  //makes the button red.
  button.style("background","#FF0000");
  //decides the font-size for the button.
  button.style("font-size","24px");
  //position for the button.
  button.position(155,100);
  //size of the button.
  button.size(300,300);
  //makes the button a circle.
  button.style("border-radius","50%");
  //when button is pressed with the mouse, it will go to the function "change".
  button.mousePressed(change);

//webcam
  //creates a capturing of the webcam-livefeed.
  camcorder = createCapture(VIDEO);
  //the size of the captured window.
  camcorder.size(800,800);
  //hides the "second" window without the desired effect.
  camcorder.hide();

}

//what happens when button is pressed:
function change() {
//hides the button when pushed.
button.hide();
//size of the captured image from the webcam.
image(camcorder, 0, 0, 640, 480);

//text
fill(255,0,0);
textSize(20);
textStyle(BOLD);
text("YOU'VE BEEN BUSTED FOR TRYING TO ERASE",105,50);
text("PROPERTY OF THE GOVERNMENT!",155,70);
textSize(20);
text("THIS IMAGE WILL NOW BE FORWARDED",135,435);
text("TO THE HEAD OF AUTHORITY",185,460);


}
