class Giraffe {

  constructor() {
  //speed of the movement of the giraffe (random between 2 and 4).
  this.speed = floor(random(2,4));
  //starting y-coordinates for the giraffes. random between 15 and 35).
  this.coordinate = floor(random(15,35));
  // vector = magnitude + direction.
  // the goal will in this case be a random width and at the placement of the ufo, which is the height of the canvas-10.
  this.goal = new createVector(random(width), height-10);

  }
//movement of the giraffes.
  move() {
  //the giraffes are moving down the y-axis with the decided speed.
    this.goal.y -= this.speed;
  }
//how the giraffes are displayed.
  show() {
    push();
    //makes the giraffes fly.
    translate(this.goal.x, this.goal.y);
    image(food, 0, this.coordinate, 100, 100);
    pop();
  }
}
