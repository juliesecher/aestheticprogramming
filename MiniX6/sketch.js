let ufo;
//the x position for the ufo.
let ufoPosX;
// the minimum number of giraffes on screen.
let min_giraffe = 4;
let giraffe = [];
let score = 0;
let lose = 0;
let ufoSize = 150;

function preload() {
  ufo = loadImage("ufo.png");
  food = loadImage("giraffe.png");
}

function setup() {
  createCanvas(800,800);
//starting x-coordinate for the ufo.
  ufoPosX = 300;
}

function draw() {
  background(166,244,255);
//grass
  noStroke();
  fill(26,202,55);
  rect(0,760,800,40);

//the upcoming functions.
  displayScore();
  checkGiraffeNum();
  showGiraffe();
  checkCaughtDinner();
  checkResult();

  image(ufo,ufoPosX,0,ufoSize,ufoSize);
}

function checkGiraffeNum() {
  //if there are less than the minimum number of giraffes (4) on screen, new one(s) will appear.
  if (giraffe.length < min_giraffe) {
    giraffe.push(new Giraffe());
  }
}

function showGiraffe() {
//if 'i' is less than the length of giraffes, 'i' will plus with 1, making it equal the amount of giraffes on screen.
  for (let i = 0; i < giraffe.length; i++) {
//this makes the giraffes on screen move and show like decided in the Class file.
  giraffe[i].move();
  giraffe[i].show();
  }
}

function checkCaughtDinner() {
//if 'j' is less than the length of giraffes, 'j' will plus with 1, making it equal the amount of giraffes on screen.
  for (let j = 0; j < giraffe.length; j++) {
//'d' is the distance between the x and y positions of the ufo and giraffes.
    let d = dist(ufoSize/2, ufoPosX+ufoSize/2, giraffe[j].goal.y, giraffe[j].goal.x);

//if giraffe is close enough to the ufo to get eaten, +1 on score + the eaten giraffe dissapears.
    if (d < ufoSize/2) {
      score++;
      giraffe.splice(j,1);
//if giraffe is not close enough to the ufo to get eaten, +1 on lose counter + the giraffe flies out of the screen.
} else if (giraffe[j].goal.y < 2) {
      lose++;
      giraffe.splice(j,1);
    }
  }
}

function displayScore() { //shows the number of caught/missed giraffes.
  fill(0);
  textSize(35);
  textStyle(BOLD);
  text("HELP THE ALIENS CATCH THEIR DINNER!",10,height/1.4-30);
  fill(150);
  textSize(17);
  text("You have caught " + score + " earthbeing(s)",10,height/1.4);
  text("You have missed " + lose + " earthbeing(s)",10,height/1.4+20);
  fill(0);
  text("PRESS the LEFT & RIGHT ARROW key(s) to catch the earthbeings!",10,height/1.4+40);
}

function checkResult() {
  //if lose is bigger than score and higher than 2 = game over.
  if (lose > score && lose > 2) {
    fill(255,0,0);
    textSize(90);
    textStyle(BOLD);
    text("GAME OVER", 130, 400);
    textSize(30);
    text("the aliens will starve :(",240,440);
    fill(166,244,255);
    ellipse(400,600,1000,300);
    noLoop();
  }
}

function keyPressed() {
  //when left arrow is pressed, ufo's x-position will move 50 down the x axis.
  if (keyCode == LEFT_ARROW) {
    ufoPosX-=50;
  //when right arrow is pressed, ufo's x'position will move 50 up the x axis.
  } else if (keyCode == RIGHT_ARROW) {
    ufoPosX+=50;
  }
}
