## **MINIX6 - OBJECT ABSTRACTION - SUBMISSION** 

- [**Link for my game**](https://juliesecher.gitlab.io/aestheticprogramming/MiniX6/)
- [Link for my code for MiniX6](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX6/sketch.js)
- [Link for my class-code](https://gitlab.com/juliesecher/aestheticprogramming/-/blob/main/MiniX6/Giraffe.js)

**DESCRIPTION OF MY PROGRAM**

For this MiniX we had to create a game, and at first I wasn't really sure what I wanted to create, and therefore I gathered some inspiration from [_Winnie's PacMan game_](https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction), and from that made it into my own --- a game where you have to help aliens in a ufo catch their dinner, the earthbeings (giraffes). You control the ufo using your right and left arrow keys, and have to catch the flying giraffes. If you lose, the aliens starve.

**HOW DID I CREATE MY GAME?** 

To implement data and functionality into an object, in this case the giraffes, I created a class. Here, I decided on the size, movements and so on for my giraffes. To control the way the game is played, I have used different functions. These include function _keyPressed(), if-statements, displayScore_ and so on. For the ufo and the giraffes, I have used images. 

**THE CHARACTERISTICS OF OBJECT-ORIENTED PROGRAMMING (OOP) AND THE WIDER IMPLICATIONS OF ABSTRACTION**

Object-oriented programming is characterized with being organized and concrete even though objects are abstracted. In the book ["Aaesthetic programming"](https://aesthetic-programming.net/pages/6-object-abstraction.html), it is mentioned how object-oriented programming is intended to reflect on the way things are organized and imagined from a programmer's perspective. By examining the way independent objects interact with each other, an understanding of their functions is provided. 

**MY MINIX IN A WIDER, CULTURAL CONTEXT**

As for the cultural context of my game, it is quite silly - and therefore also socially acceptable. There aren't really any controversies associated with my game _(if you really **try**, to find something, it should perhaps be animal cruelty, lol)._ Looking at my game in the light of abstracting complex details, the class I have created has made it a lot easier to store the data about the specific elements in my game, abstracting it from the rest of the coding. 

**REFLECTIONS**

This time around I found it very challenging to get it all to work. Especially the distance from the ufo to the giraffes was very difficult - but when it finally worked, it was all worth it! :)))) This is definitely the MiniX that have taken me the longest, but I also think it has been the most rewarding one. It really feels like a win, when it all works.

![](giraffegif.gif)





